﻿export default {
  data(){
    return {
      timer: ''  //toast定时器
    }
  },
  install: function (Vue, options) {
    /*获取token*/
    Vue.prototype.getToken = function () {
      return this.$route.query.token;
    }

    /*时间格式转转换*/
    Vue.prototype.format = function (str) {
      var date = new Date(str.replace(/-/g, '/')),
        t = {
          y: date.getFullYear(),
          M: date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1,
          d: date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
          h: date.getHours() < 10 ? '0' + date.getHours() : date.getHours(),
          m: date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
        }
      return t.y + '.' + t.M + '.' + t.d + ' ' + t.h + ':' + t.m
    }

    /*吐司提示*/
    Vue.prototype.toast = function (msg, time) {
      time = time ? time : 1500;
      var toast = document.getElementsByClassName('toast-alert')[0];
      if (toast) {
        document.body.removeChild(toast);
        clearTimeout(this.timer);
      }
      var newToast = document.createElement("div");
      newToast.className = 'toast-alert';
      newToast.innerHTML = "<div><div class='dv1'><div class='dv2'>" + msg + "</div></div></div>"
      // newToast.innerHTML = msg;
      document.body.appendChild(newToast);
      this.timer = setTimeout(function () {
        document.body.removeChild(newToast);
      }, time)
    }

    /*手机号截取显示*/
    Vue.prototype.cutPhone = function (teltxt) {
      var changeTel = teltxt.substring(0, 3) + "****" + teltxt.substring(7, 11);
      return changeTel;
    }

    /*显示万过滤器*/
    Vue.filter("sum", function (value) {
      if (value >= 100000) {
        value = Math.floor(value / 10000) + '万';
      }
      return value
    });

    /*请求封装*/
    Vue.prototype.asyncHttp = function (method, url, params, success, error) {
      this.$http({
        method: method,
        url: url,
        params: params,
        headers: {TOKEN: "xotaLuIW6QE0QWBvUBDiIUVC9PHm08OcEQk/aPDxDQxcfro55OLK9/PwELIFKa0k9qsX98GKVj8QYOjdJR5E1A=="}
      }).then(function (res) {
        var data = res.body;
        if (data.status == 200) {
          success(data.data);
        } else {
          this.toast(data.msg);
        }
      }, function () {
        if (error) {

        }
      })
    }

    /*滚动到底部加载更多*/
    Vue.prototype.scrollMore = function (callback) {
      window.onscroll = function () {
        var scrollTop = document.body.scrollTop,
          scrollHeight = document.body.scrollHeight,
          clientHeight = document.documentElement.clientHeight;
        if (scrollHeight - scrollTop - clientHeight <= 30) {
          callback();
        }
      }
    }

    /*判断android还是IOS*/
    Vue.prototype.clientType = function (adrCallback, iosCallback) {
      var u = navigator.userAgent;
      if (u.indexOf('Android') > -1 || u.indexOf('Adr') > -1) {
        if (adrCallback) {
          adrCallback();
        }
      } else if (!!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
        if (iosCallback) {
          iosCallback();
        }
      }
    }

    /*/!*设置默认图片*!/
     Vue.prototype.defaultImg = function (option) {
     var defaultImg = '';
     switch (option) {
     case 0:
     defaultImg = 'this.src = "' + require('../../images/loadError01.png') + '"';
     break;
     case 1:
     defaultImg = 'this.src = "' + require('../../images/loadError02.png') + '"';
     break;
     case 2:
     var n = Math.floor(Math.random() * 3);
     defaultImg = 'this.src = "' + require('../../images/head-photo' + n + '.png') + '"';
     break;
     }
     return defaultImg;
     }*/

    /*弹出框显示时，禁止底层滑动*/
    Vue.prototype.setSlide = function (option) {
      var html = document.documentElement, body = document.body, scrollTop = body.scrollTop;
      this.clientType(function () {
        if (option === 0) {
          html.style.overflow = 'hidden';
          body.style.overflow = 'hidden';
        } else {
          html.style.overflowY = 'scroll';
          body.style.overflowY = 'scroll';
        }
      })
      if (option === 0) {
        body.className = 'body-fixed';
        body.style.top = scrollTop * -1 + 'px';
      } else {
        body.className = '';
        body.scrollTop = body.style.top.slice(0, -2) * -1;
        body.style.top = null;
      }
    }

    /*返回按钮设置*/
    Vue.prototype.goBack = function () {
      if (this.$route.query.native == 0) {
        this.$router.go(-1);
      } else {
        if (this.$route.query.router && this.$route.query.router.indexOf('native=0') > -1) {
          this.$router.go(-1);
        } else {
          this.clientType(function () {
            window.Android.startFunctionBack();
          }, function () {
            window.webkit.messageHandlers.AppModel.postMessage('back');
          })
        }
      }
    }
  }
}
