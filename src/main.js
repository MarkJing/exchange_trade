import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './router'
import VueResource from 'vue-resource'
import publicMethod from './config/public'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(publicMethod)

const router = new VueRouter({
  routes
})

new Vue({
  router
}).$mount('#app')   //手动挂载
