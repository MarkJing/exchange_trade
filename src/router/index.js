import App from '../App.vue'
// find
import findIndex from '../page/find/findIndex.vue'
import search from '../page/find/search.vue'
import newsDetails from '../page/find/newsDetails.vue'
import notice from '../page/find/notice.vue'
// follow
import followIndex from '../page/follow/followIndex.vue'
import followExchange from '../page/follow/followExchange.vue'
// more
import capitalFlowRecord from '../page/more/capitalFlowRecord.vue'
import badgeTask from '../page/more/badgeTask.vue'
import positionStatistics from '../page/more/positionStatistics.vue'
import masterIndex from '../page/more/masterIndex.vue'
// order
import orderIndex from '../page/order/orderIndex.vue'
// market
import market from '../page/market/market.vue'

export default[{
  path: '/',
  component: App, //顶层路由，对应index.html
  children: [ //二级路由。对应App.vue
    //地址为空时跳转findIndex页面
    {
      path: '',
      redirect: '/findIndex'
    },
    //发现主页
    {
      path: '/findIndex',
      component: findIndex
    },
    //搜索结果页
    {
      path: '/search',
      component: search
    },
    //新闻详情页面
    {
      path: '/newsDetails',
      component: newsDetails
    },
    //跟单首页
    {
      path: '/followIndex',
      component: followIndex
    },
    //跟单交易
    {
      path: '/followExchange',
      component: followExchange
    },
    //出入金纪录
    {
      path: '/capitalFlowRecord',
      component: capitalFlowRecord
    },
    //持仓统计
    {
      path: '/positionStatistics',
      component: positionStatistics
    },
    //徽章任务
    {
      path: '/badgeTask',
      component: badgeTask
    },
    //订单首页
    {
      path: '/orderIndex',
      component: orderIndex
    },
    //大师主页
    {
      path: '/masterIndex',
      component: masterIndex
    },
    //行情
    {
      path: '/market',
      component: market
    },
    //通知
    {
      path: '/notice',
      component: notice
    }
  ]
}]
